export const PostSchema = {
  bsonType: 'object',
  required: ['_id', 'createdBy', 'words'],
  properties: {
    _id: {
      bsonType: 'objectId',
      description: 'must be a objectId and is required',
    },
    createdBy: {
      bsonType: 'string',
      description: 'must be a string and is required',
    },
    words: {
      bsonType: 'array',
      items: {
        bsonType: 'object',
        required: ['name', 'translation', 'createdAt'],
        properties: {
          name: {
            bsonType: 'string',
            description: 'must be a string and is required',
          },
          translation: {
            bsonType: 'string',
            description: 'must be a string and is required',
          },
          createdAt: {
            bsonType: 'number',
            description: 'must be a number and is required',
          },
        },
      },
      additionalItems: false,
    },
  },
  additionalProperties: false,
};
