'use server';

import { getSession } from '@auth0/nextjs-auth0';
import { redirect } from 'next/navigation';

import { createUser, getUser } from '@/lib/mongodb';

import Home from '@/templates/Home';

import { Data } from '@/types/general';

async function HomePage() {
  const session = await getSession();
  if (!session) {
    redirect('/login');
  }
  let dbUser = await getUser(session.user.sub);
  dbUser = JSON.parse(JSON.stringify(dbUser)) as Data;
  if (!dbUser) {
    await createUser(session.user.sub);
    return <Home data={[]} />;
  }
  return <Home data={dbUser.words.reverse()} />;
}

export default HomePage;
