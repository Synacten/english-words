import { ReactNode } from 'react';
import * as React from 'react';

import '@/styles/globals.css';

import Loader from '@/components/Loader';

export const metadata = {
  description: 'platform for learning words',
  title: 'My vocabulary',
  icons: { icon: '/favicon/favicon.svg' },
};

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang='en'>
      <body>
        <Loader />
        <div id='main-container' className='relative'>
          {children}
        </div>
      </body>
    </html>
  );
}
