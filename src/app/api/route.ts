import { getSession, Session, withApiAuthRequired } from '@auth0/nextjs-auth0';
import { MongoError } from 'mongodb';
import { NextRequest, NextResponse } from 'next/server';
import 'server-only';

import { initWordsCollection } from '@/lib/mongodb';

import { ErrorFromServer } from '@/types/general';

export const POST = withApiAuthRequired(async function postFunc(
  req: NextRequest
) {
  const res = new NextResponse();
  const { user } = (await getSession(req, res)) as Session;
  try {
    const body = await req.json();
    for (const bodyKey in body) {
      body[bodyKey] = body[bodyKey].trim();
    }
    const newItem = {
      ...body,
      createdAt: Date.now(),
    };
    const db = await initWordsCollection();
    const data = await db.updateOne(
      { createdBy: user.sub },
      { $push: { words: newItem } }
    );
    return NextResponse.json(data);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
    const error: ErrorFromServer = {
      error: 'Сталася невідома помилка',
    };
    if (e instanceof MongoError && e.name === 'MongoServerError') {
      error.error = e.errmsg;
      return NextResponse.json(error, { status: 422 });
    }
    return NextResponse.json(error, { status: 500 });
  }
});

export const DELETE = withApiAuthRequired(async function deleteFunc(
  req: NextRequest
) {
  const res = new NextResponse();
  const { user } = (await getSession(req, res)) as Session;
  try {
    const body: { createdAt: string } = await req.json();
    const db = await initWordsCollection();
    const data = await db.updateOne(
      { createdBy: user.sub },
      { $pull: { words: body } }
    );
    return NextResponse.json(data);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
    const error: ErrorFromServer = {
      error: 'Сталася невідома помилка',
    };
    if (e instanceof MongoError && e.name === 'MongoServerError') {
      error.error = e.errmsg;
      return NextResponse.json(error, { status: 422 });
    }
    return NextResponse.json(error, { status: 500 });
  }
});
