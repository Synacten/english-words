import * as React from 'react';

function Page() {
  return (
    <main>
      <section className='bg-white'>
        <div className='layout flex min-h-screen flex-col items-center justify-center text-center text-black'>
          <h1 className='mt-8 text-2xl md:text-4xl'>
            Sorry but you need to be authenticated for continue
          </h1>
          <a
            href='/api/auth/login'
            className='border-1 mt-5 border px-5 py-3 transition-colors hover:bg-blue-500 hover:text-white'
          >
            Login
          </a>
        </div>
      </section>
    </main>
  );
}

export default Page;
