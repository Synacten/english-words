'use client';

import { useEffect, useState } from 'react';

import Form from '@/components/Form';
import WordsShow from '@/components/WordsShow';

import { Words } from '@/types/general';

function Home({ data }: { data: Words[] }) {
  const [localData, setLocalData] = useState<Words[]>(data);

  useEffect(() => {
    setLocalData(data);
  }, [data]);

  const handleRandomize = () => {
    setLocalData([...randomize(data)]);
  };
  const handleSortByDate = () => {
    setLocalData([...data]);
  };

  return (
    <main className='layout py-14 sm:py-20'>
      <div className='grid grid-cols-1 items-center justify-between pb-6 sm:flex'>
        <div className='flex items-center gap-4'>
          <button
            onClick={handleRandomize}
            type='button'
            className='border-dark border px-5 py-3 transition-colors hover:bg-blue-500 hover:text-white'
          >
            Randomize
          </button>
          <button
            onClick={handleSortByDate}
            type='button'
            className='border-dark border px-5 py-3 transition-colors hover:bg-blue-500 hover:text-white'
          >
            Sort by date
          </button>
        </div>
        <a
          className='border-dark row-start-1 mb-6 border px-5 py-3 text-center hover:bg-yellow-400 sm:row-auto sm:mb-0'
          href='/api/auth/logout'
        >
          Change account
        </a>
      </div>
      <div className='grid grid-cols-1 gap-8 md:grid-cols-[1fr_minmax(300px,_auto)] xl:grid-cols-[1fr_minmax(350px,_auto)]'>
        <WordsShow data={localData} />
        <Form />
      </div>
    </main>
  );
}

export default Home;

function randomize(data: Words[]) {
  const newData = [...data];
  return newData.sort(() => Math.random() - 0.5);
}
