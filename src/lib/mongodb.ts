import { MongoClient, ObjectId } from 'mongodb';
import * as process from 'process';
import 'server-only';

import { PostSchema } from '@/models/post';

if (!process.env.MONGODB_URI) {
  throw new Error('Invalid/Missing environment variable: "MONGODB_URI"');
}

const uri = process.env.MONGODB_URI;
const options = {};

let client;
export let clientPromise: Promise<MongoClient>;

if (process.env.NODE_ENV === 'development') {
  // In development mode, use a global variable so that the value
  // is preserved across module reloads caused by HMR (Hot Module Replacement).
  const globalWithMongo = global as typeof globalThis & {
    _mongoClientPromise?: Promise<MongoClient>;
  };

  if (!globalWithMongo._mongoClientPromise) {
    client = new MongoClient(uri, options);
    globalWithMongo._mongoClientPromise = client.connect();
  }
  clientPromise = globalWithMongo._mongoClientPromise;
} else {
  // In production mode, it's best to not use a global variable.
  client = new MongoClient(uri, options);
  clientPromise = client.connect();
}

export async function initAppDbPromise() {
  const client = await clientPromise;
  return client.db(process.env.MONGODB_DATABASE_NAME);
}

export async function initWordsCollection() {
  const db = await initAppDbPromise();
  const exist = await db.command({
    listCollections: 1.0,
    authorizedCollections: true,
    nameOnly: true,
  });
  const existCollectionWord = exist.cursor.firstBatch.find(
    (value: { name?: string }) => value.name === 'words'
  );

  if (existCollectionWord) {
    const collection = db.collection('words');
    await db.command({
      collMod: 'words',
      validator: {
        $jsonSchema: PostSchema,
      },
    });
    return collection;
  } else {
    return await db.createCollection('words', {
      validator: {
        $jsonSchema: PostSchema,
      },
    });
  }
}

export async function getUser(sub: string) {
  const db = await initAppDbPromise();
  return db.collection('words').findOne({ createdBy: sub });
}

export async function createUser(sub: string) {
  const db = await initAppDbPromise();
  return db
    .collection('words')
    .insertOne({ _id: new ObjectId(), createdBy: sub, words: [] });
}
