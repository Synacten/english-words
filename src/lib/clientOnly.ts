import 'client-only';

export function addLoader() {
  const loader = document.querySelector('#loader');
  const mainContainer = document.querySelector('#main-container');
  const elements = document.querySelectorAll('a, button, input, textarea');
  elements.forEach((element) => {
    element.setAttribute('tabindex', '-1');
  });
  if (
    !loader ||
    !mainContainer ||
    !(loader instanceof HTMLElement) ||
    !(mainContainer instanceof HTMLElement)
  ) {
    return;
  }
  loader.style.display = 'flex';
  document.body.style.overflow = 'hidden';
  mainContainer.style.zIndex = '-10';
}
export function removeLoader() {
  const loader = document.querySelector('#loader');
  const mainContainer = document.querySelector('#main-container');
  const elements = document.querySelectorAll('a, button, input, textarea');
  elements.forEach((element) => {
    element.setAttribute('tabindex', '0');
  });
  if (
    !loader ||
    !mainContainer ||
    !(loader instanceof HTMLElement) ||
    !(mainContainer instanceof HTMLElement)
  ) {
    return;
  }
  loader.style.display = 'none';
  document.body.style.overflow = 'unset';
  mainContainer.style.zIndex = '0';
}
