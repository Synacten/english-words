'use client';
import { useRouter } from 'next/navigation';

import { addLoader, removeLoader } from '@/lib/clientOnly';

import { Words } from '@/types/general';

import TrashIcon from '~/svg/Trash.svg';

function WordsShow({ data }: { data: Words[] }) {
  const router = useRouter();
  const handleDelete = async (dataObj: Words) => {
    const isConfirm = confirm(
      `Do you really want delete word "${dataObj.name}"?`
    );
    if (!isConfirm) {
      return;
    }
    addLoader();
    fetch('/api', {
      method: 'DELETE',
      body: JSON.stringify({ createdAt: dataObj.createdAt }),
    }).then(() => {
      router.refresh();
      removeLoader();
    });
  };

  if (data.length <= 0) {
    return (
      <article className='border-dark border p-4'>
        There are no entries yet
      </article>
    );
  }
  return (
    <table>
      <thead>
        <tr className='grid grid-cols-2'>
          <th className=''>Original</th>
          <th className=''>Translation</th>
        </tr>
      </thead>
      <tbody>
        {data.map((obj, index) => (
          <tr key={index} className='grid grid-cols-[1fr_1fr_auto] '>
            <td className='border-dark flex items-center border-[0.5px] p-2'>
              {obj.name}
            </td>
            <td className='border-dark flex items-center border-[0.5px] p-2'>
              {obj.translation}
            </td>
            <td className='border-dark flex items-center border-[0.5px]'>
              <button
                onClick={() => handleDelete(obj)}
                type='button'
                aria-label='delete button'
                className='h-full w-full p-2'
              >
                <TrashIcon className='h-6 w-6' />
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default WordsShow;
