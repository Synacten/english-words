import LoaderSvg from '~/svg/Loader.svg';

function Loader() {
  return (
    <div
      tabIndex={1}
      id='loader'
      className='pointer-events-none fixed left-0 top-0 z-40 hidden min-h-screen min-w-full items-center justify-center overflow-hidden bg-black/[.8]'
    >
      <LoaderSvg className='h-20 w-20' />
    </div>
  );
}

export default Loader;
