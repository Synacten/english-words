'use client';

import { useRouter } from 'next/navigation';
import { BaseSyntheticEvent, useState } from 'react';

import { addLoader, removeLoader } from '@/lib/clientOnly';
import { cn } from '@/lib/utils';

function Form() {
  const router = useRouter();
  const [inputName, setInputName] = useState<string>('');
  const [inputTranslation, setInputTranslation] = useState<string>('');
  const [error, setError] = useState<boolean>(false);
  const [message, setMassage] = useState<string>('');

  const handleSubmit = (e: BaseSyntheticEvent) => {
    e.preventDefault();
    if (!inputName || !inputTranslation) {
      setError(true);
      setMassage('fields are required');
      return;
    }
    addLoader();
    setError(false);
    setMassage('');
    handleClearFields();
    fetch('/api', {
      method: 'POST',
      body: JSON.stringify({ name: inputName, translation: inputTranslation }),
    })
      .then((response) => {
        if (response.status === 401) {
          router.push('/api/auth/login');
          setError(true);
          setMassage('Server Error');
        }
        if (response.status !== 200) {
          setError(true);
          setMassage('Server Error');
          return;
        }
        setError(false);
        setMassage('Word has added success');
        router.refresh();
        removeLoader();
      })
      .catch(() => {
        setError(true);
        setMassage('Server Error');
        removeLoader();
      });
  };
  const handleChangeName = (e: BaseSyntheticEvent) => {
    setInputName(e.target.value);
  };
  const handleChangeTranslation = (e: BaseSyntheticEvent) => {
    setInputTranslation(e.target.value);
  };

  const handleClearFields = () => {
    setInputName('');
    setInputTranslation('');
  };

  return (
    <form onSubmit={handleSubmit} className='grid h-fit grid-cols-1 gap-4'>
      <div className='grid grid-cols-1'>
        <label className='text-sm' htmlFor='name'>
          English word
        </label>
        <input
          id='name'
          name='name'
          type='text'
          onChange={handleChangeName}
          value={inputName}
        />
      </div>
      <div className='grid grid-cols-1'>
        <label className='text-sm' htmlFor='translation'>
          translate word
        </label>
        <input
          id='translation'
          name='translation'
          type='text'
          onChange={handleChangeTranslation}
          value={inputTranslation}
        />
      </div>
      {message && (
        <p className={cn(error ? 'text-red-600' : 'text-green-600')}>
          {message}
        </p>
      )}
      <button
        type='submit'
        className='border-dark border p-3 transition-colors hover:bg-blue-500 hover:text-white'
      >
        Submit
      </button>
    </form>
  );
}

export default Form;
