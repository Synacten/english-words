import { ObjectId } from 'mongodb';

export interface ErrorFromServer {
  error: string;
}

export interface Words {
  name: string;
  translation: string;
  createdAt: number;
}

export interface Data {
  _id: ObjectId;
  createdBy: string;
  words: Words[];
}
