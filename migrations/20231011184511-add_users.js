// eslint-disable-next-line @typescript-eslint/no-var-requires
const { ObjectId } = require('mongodb');
module.exports = {
  async up(db) {
    const all = await db.collection('words').find().sort({ _id: -1 }).toArray();
    const newUser = {
      _id: new ObjectId(),
      createdBy: 'sub',
      words: all,
    };
    await db.collection('words').drop();
    await db.collection('words').insertOne(newUser);
  },

  async down() {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  },
};
