module.exports = {
  async up(db) {
    const all = await db.collection('words').find().toArray();
    all.forEach((item) => item.words.reverse());
    const changedArray = all.map((item) => {
      return {
        ...item,
        words: item.words.map((wordItem) => {
          return {
            name: wordItem.name,
            translation: wordItem.translation,
            createdAt: wordItem.createdAt,
          };
        }),
      };
    });
    await db.collection('words').drop();
    await db.collection('words').insertMany(changedArray);
  },

  async down() {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  },
};
